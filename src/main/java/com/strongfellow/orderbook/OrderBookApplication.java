package com.strongfellow.orderbook;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.strongfellow.waterpolo.ClientBuilder;
import com.strongfellow.waterpolo.OpenClient;
import com.strongfellow.waterpolo.api.data.OrderSummary;
import com.strongfellow.waterpolo.push.OrderBook;
import com.strongfellow.waterpolo.push.OrderBookFactory;
import com.strongfellow.waterpolo.push.OrderBookSummary;

public class OrderBookApplication {

    private static class Params {

        @Parameter(names = "--currency-pair")
        public String currencyPair = "BTC_ETH";

        @Parameter(names="--precision")
        public Integer precision = 4;
    }

    private static final Logger logger = LoggerFactory.getLogger(OrderBookApplication.class);

    public static void main(String[] args) throws Exception {
        logger.info("begin order book");

        final Params params = new Params();
        final JCommander jc = new JCommander(params, args);
        final String currencyPair = params.currencyPair;

        final int digits = params.precision;


        final OpenClient openClient = new ClientBuilder().getOpenClient();

        final OrderBook orderBook = new OrderBookFactory()
                .withCurrencyPair(currencyPair)
                .withClient(openClient).build();
        final BigDecimal precision = BigDecimal.valueOf(Math.pow(10.0, (-1 * digits)));
        while (true) {
            final BigDecimal depth = BigDecimal.valueOf(1000.0);
            final OrderBookSummary obs = orderBook.getSummary(precision, depth);
            OrderBookApplication.printOrderBookSummary(currencyPair, digits, obs);
            Thread.sleep(500);
        }
    }

    private static void printOrderBookSummary(String currencyPair, int digits, OrderBookSummary obs) {
        System.out.print("\033[H\033[2J");
        System.out.flush();

        System.out.format("\n                    %s ASKS\n\n", currencyPair);
        OrderBookApplication.printOrders(digits, obs.getAsks());

        System.out.format("\n                    %s BIDS\n\n", currencyPair);
        OrderBookApplication.printOrders(digits, obs.getBids());
    }

    private static void printOrders(int digits, Iterable<OrderSummary> asks) {
        double acc = 0.0;
        int i = 0;
        for (final OrderSummary order : asks) {
            final double inc = order.getMarketAmount().doubleValue();
            acc += inc;
            final double[] numbers = new double[] {
                    order.getAssetAmount().doubleValue(),
                    inc,
                    acc
            };
            System.out.format(" %." + digits + "f", order.getPrice());

            for (final double d : numbers) {
                final String a = String.format(" %.8f ", d);
                System.out.format("%16s", a);
            }
            System.out.format("\n");
            if (i++ > 9) {
                return;
            }
        }
    }


}
